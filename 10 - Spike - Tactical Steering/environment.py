# environment.py
from graphics import egi, KEY
from vector2d import Vector2D
from math import sqrt

class environment():
  
  def __init__(self, px, py, prad): # parameter x and y and parameter radius
    self.pos = Vector2D(px, py)
    self.radius = prad
    self.color = 'BLUE'

  def render(self):
    # sets color i guess
    egi.set_pen_color(name=self.color)
    # we are just copying this bit from agent.py
    egi.set_stroke(2)
    # pos[], radius, other stuff we won't set
    egi.circle(self.pos, self.radius)

    #egi.cross(self.pos, 10)

  def get_hiding_spot(self, hunter_pos):
    vx = self.pos.x - hunter_pos.x
    vy = self.pos.y - hunter_pos.y
    length = sqrt(vx*vx + vy*vy)
    return Vector2D(vx / length * 90 + self.pos.x, vy / length * 90 + self.pos.y)

