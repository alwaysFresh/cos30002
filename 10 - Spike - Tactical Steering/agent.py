'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform # half of this stuff has already been done
from path import Path

AGENT_MODES = {
    KEY._1: 'prey',
    KEY._2: 'hunter',
}

class Agent(object):

    def __init__(self, world=None, scale=30.0, mass=1.0, mode='prey'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass

        # data for drawing this agent
        self.color = 'ORANGE'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

        ### wander details
        self.wander_target = Vector2D(1, 0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale

        # limits?
        self.max_speed = 20.0 * scale
        self.max_force = 500.0

        # TASK 10: modifying properties of prey agent 
        if self.mode == 'prey':
            self.best_hiding_spot = Vector2D()
            self.hiding_spots = [self.best_hiding_spot]
            self.max_speed = 10.0 * scale
            self.max_force = 300.0
            self.color = 'PINK'
            self.vehicle_shape = [
              Point2D(-0.5, 0.3),
              Point2D(0.5, 0.0),
              Point2D(-0.5, -0.3)       
            ]
                  # debug draw info?
        self.show_info = False

    def calculate(self, delta):
            # calculate the current steering force
        mode = self.mode
        # prey mode, will try to flee from the hunter and hide behind circles
        if mode == 'prey':
            force = self.prey()
        # hunter mode, for now it just wanders
        elif mode == 'hunter':
            force = self.hunter(delta)
        else:
            force = Vector2D()
        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        ## force = self.calculate()
        force = self.calculate(delta)  # <-- delta needed for wander | thannkyou!
        force.truncate(self.max_force)
        ## limit force? <-- for wander
        # ...
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        
        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

        # draw wander info?
        if self.mode == 'hunter':
            # calculate the center of the wander circle in front of the agent
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            # draw the wander circle
            egi.green_pen()
            egi.circle(wld_pos, self.wander_radius)
            # draw the wander target (little circle on the big circle
            egi.red_pen()
            wnd_pos = (self.wander_target +  Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            egi.circle(wld_pos, 3)



        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

        # TASK 10 SPIKE: prey rendering (basically just rendering the best hiding spot 
        if self.mode == 'prey' and len(self.world.agents) > 1:
            egi.red_pen()
            for agent in self.world.agents:
                if agent.mode == 'hunter':
                    # draw line from hunder to each hiding spot
                    for env in self.world.environments:
                        b = env.get_hiding_spot(agent.pos)
                        egi.line_by_pos(agent.pos, b)
            
            #bhs = min(hiding_spots, key=lambda x: x.distance(self.pos))
            egi.aqua_pen()
            egi.cross(self.best_hiding_spot, 10)
            for h in self.hiding_spots:
                if h != self.best_hiding_spot:
                    egi.red_pen()
                    egi.cross(h, 10)


    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)


    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target

        jitter_tts = self.wander_jitter * delta
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius
        target = wt + Vector2D(self.wander_dist, 0)
        # it is terrible practice to have two separate scale values my bad (30 parameter)
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        return self.seek(wld_target)
    
    # accessing the prey_pos list comprehension by index is bad practice, not ideal
    def hunter(self, delta):
        # select a prey
        preys = [a for a in self.world.agents if a.mode == 'prey']
        ret = Vector2D()
        # first, we will delete the prey if we have caught it 
        if len(preys) > 0:
            prey = preys.pop()
            if self.pos.distance(prey.pos) < 10:
                self.world.agents.remove(prey)    
                print("YUM")
            # check if it can see the prey (not blocked by cirlce) and in range
            elif self.can_see(prey.pos) and self.pos.distance(prey.pos) < 200:
                ret = self.seek(prey.pos)
            # else we just wander
            else:
                ret = self.wander(delta)
            
        return ret

    def prey(self):
        # get the best hiding spot and plug it into seek
        self.hiding_spots = []
        for agent in self.world.agents:
            if agent.mode == 'hunter':
                for env in self.world.environments:
                    b = env.get_hiding_spot(agent.pos)
                    self.hiding_spots.append(b)
        if len(self.world.agents) > 1:
            self.best_hiding_spot = min(self.hiding_spots, key=lambda x: x.distance(self.pos))
            return self.seek(self.best_hiding_spot)
        else:
            return self.seek(self.world.target)

    def can_see(self, pos):
        # we have three points, hunter, prey & circle
        ret = True
        for env in self.world.environments:
            dxc = env.pos.x - self.pos.x
            dyc = env.pos.y - self.pos.y

            dxl = pos.x - self.pos.x
            dyl = pos.y - self.pos.y
            
            cross = dxc * dyl - dyc * dxl

            threshold = 90
            if (abs(cross) < threshold):
                ret = False
        return ret
'''

if self.mode == 'prey' and len(self.world.agents) > 1:
            hiding_spots = []
            egi.red_pen()
            for agent in self.world.agents:
                if agent.mode == 'hunter':
                    # draw line from hunder to each hiding spot
                    for env in self.world.environments:
                        b = env.get_hiding_spot(agent.pos)
                        egi.line_by_pos(agent.pos, b)
                        hiding_spots.append(b)
            
            # draw x's at each hiding spot (I am quite proud of this one)
            bhs = min(hiding_spots, key=lambda x: x.distance(self.pos))
            egi.aqua_pen()
            egi.cross(bhs, 10)
            for h in hiding_spots:
                if h != bhs:
                    egi.red_pen()
                    egi.cross(h, 10)
'''

# some old code I replaced
"""
# TASK 10 SPIKE: prey rendering (basically just rendering the best hiding spot 
        if self.mode == 'prey' and len(self.world.agents) > 1:
            egi.red_pen()
            bhs = Vector2D(-1000, -1000)
            for agent in self.world.agents:
                if agent.mode == 'hunter':
                    for env in self.world.environments:
                        b = env.get_hiding_spot(agent.pos)
                        egi.line_by_pos(agent.pos, b)
                        if self.pos.distance(b) < self.pos.distance(bhs):
                            bhs = b
                            egi.aqua_pen()
                            egi.cross(bhs, 10)
                            egi.red_pen()
                        else:
                            egi.red_pen()
                            egi.cross(b, 10)

"""


'''
egi.aqua_pen()
hiding_spots = []
for agent in self.world.agents:
    if agent.mode == 'hunter':
        for env in self.world.environments:
            hiding_spots.append(env.get_hiding_spot(agent.pos))
#bhs = min([abs((self.pos.x - h.x)+(self.pos.y)) for h in hiding_spots])
bhs = hiding_spots[0]
for h in hiding_spots:
    if self.pos.distance(h) < self.pos.distance(bhs):
        bhs = h
egi.cross(bhs, 30)
'''

'''
# TASK 10 SPIKE: render the line between this and the environment :) 
for env in self.world.environments:
# i learnt this from 
# https://gamedev.stackexchange.com/questions/80277/how-to-find-point-on-a-circle-thats-opposite-another-point
# we put all the calculations in this function so the prey can get its hiding spot too
b = env.get_hiding_spot(self.pos)
egi.line_by_pos(self.pos, b)
# now we draw a convenient x
egi.cross(b, 10)
'''

'''
hiding_spots = []
egi.red_pen()
for agent in self.world.agents:
if agent.mode == 'hunter':
    # draw line from hunder to each hiding spot
    for env in self.world.environments:
        b = env.get_hiding_spot(agent.pos)
        egi.line_by_pos(agent.pos, b)
        hiding_spots.append(b)

# draw x's at each hiding spot (I am quite proud of this one)
'''

