'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians, asin, sqrt
from random import random, randrange, uniform # half of this stuff has already been done
from path import Path
from bullet import bullet

AGENT_MODES = {
    KEY._1: 'prey',
    KEY._2: 'hunter',
}

class Agent(object):

    def __init__(self, world=None, scale=30.0, mass=1.0, mode='prey'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass

        # data for drawing this agent
        self.color = 'ORANGE'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

        ### wander details
        self.wander_target = Vector2D(1, 0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale

        # limits?
        self.max_speed = 20.0 * scale
        self.max_force = 500.0

        # TASK 14 stuff
        if self.mode == 'hunter':
            self.accurate = True 
            self.bullet_speed = 20
            self.b_radius = 5
            self.b_adjust = self.bullet_speed * 2
            self.max_force = 0.0
            self.pos = Vector2D(900, 900)

            self.bullets = []
            self.rate_of_fire = 1
            self.time_to_shoot = 0
             
        if self.mode == 'prey':
            self.point_1 = Vector2D(100, 100)
            self.point_2 = Vector2D(100, 900)
            self.point_to_seek = self.point_1

        # TASK 10: modifying properties of prey agent 
        if self.mode == 'prey':
            self.best_hiding_spot = Vector2D()
            self.hiding_spots = [self.best_hiding_spot]
            self.max_speed = 10.0 * scale
            self.max_force = 300.0
            self.color = 'PINK'
            self.vehicle_shape = [
              Point2D(-0.5, 0.3),
              Point2D(0.5, 0.0),
              Point2D(-0.5, -0.3)       
            ]
                  # debug draw info?
        self.show_info = False

    def calculate(self, delta):
            # calculate the current steering force
        mode = self.mode
        # prey mode, will try to flee from the hunter and hide behind circles
        if mode == 'prey':
            force = self.prey()
            self.bullets = []
        # hunter mode, for now it just wanders
        elif mode == 'hunter':
            force = self.hunter(delta)
        else:
            force = Vector2D()
        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        ## force = self.calculate()
        force = self.calculate(delta)  # <-- delta needed for wander | thannkyou!
        force.truncate(self.max_force)
        ## limit force? <-- for wander
        # ...
        # TASK 14 
        # this code runs slower than I do (I hurt my knee)
        if self.mode == 'hunter':
            self.time_to_shoot += self.rate_of_fire 
            for bullet in self.bullets:
                bullet.update()
                for agent in self.world.agents:
                    if agent.mode == 'prey':
                        if bullet.pos.distance(agent.pos) < self.b_radius * 4:
                            self.world.agents.remove(agent)
            
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        
        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

        # draw wander info?
        if self.mode == 'hunter':
            for bullet in self.bullets:
                bullet.render()

        if self.mode == 'prey':
            egi.line_by_pos(self.pos, self.pos + self.vel)
        
        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

        


    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)


    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target

        jitter_tts = self.wander_jitter * delta
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius
        target = wt + Vector2D(self.wander_dist, 0)
        # it is terrible practice to have two separate scale values my bad (30 parameter)
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        return self.seek(wld_target)
    
    # accessing the prey_pos list comprehension by index is bad practice, not ideal
    def hunter(self, delta):
        target = Vector2D()
        for agent in self.world.agents:
            if agent.mode == 'prey':
                target = agent 
        if self.time_to_shoot >= 100:
            self.shoot(target)
            self.time_to_shoot = 0
        ret = Vector2D()
        return ret

    def prey(self):
        if self.pos.distance(self.point_1) < 20:
            self.point_to_seek = self.point_2
        elif self.pos.distance(self.point_2) < 20:
            self.point_to_seek = self.point_1
        return self.seek(self.point_to_seek)
    
    # not using in TASK14 but I might later hehe 
    def can_see(self, pos):
        # we have three points, hunter, prey & circle
        ret = True
        for env in self.world.environments:
            dxc = env.pos.x - self.pos.x
            dyc = env.pos.y - self.pos.y

            dxl = pos.x - self.pos.x
            dyl = pos.y - self.pos.y
            
            cross = dxc * dyl - dyc * dxl

            threshold = 90
            if (abs(cross) < threshold):
                ret = False
        return ret

    def shoot(self, target):
        # we need to adjust velocity to predict where the agent will be 
        pos = Vector2D(self.pos.x, self.pos.y) 

        vel = self.predictPos(target)
        # 20 fast, 10 slow
        self.bullets.append(bullet(pos, vel, self.bullet_speed, self.b_radius))  


    def predictPos(self, target):
        toTarget = target.pos - self.pos
        # make shot less acurate if needed
        if not self.accurate:
            toTarget += Vector2D(randrange(-300, 300), randrange(-300, 300))

        lookAheadTime = toTarget.length() / ((self.bullet_speed * self.b_adjust) + target.max_speed)
        lookAheadPos = target.pos + (target.vel * lookAheadTime)
        egi.line_by_pos(self.pos, lookAheadPos)
        return self.seek(lookAheadPos)

