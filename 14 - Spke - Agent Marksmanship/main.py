#!/usr/bin/env python3
# main.py
'''Autonomous Agent Movement: Seek, Arrive and Flee

Created for COS30002 AI for Games, Lab,
by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without
permission.

Notes:
* The graphics.py module provides a simple wrapper around pyglet to make
  things easier - hence Easy Graphics Interface (egi).

* If you want to respond to a key press, see the on_key_press function.
* The world contains the agents. In the main loop we tell the world
  to update() and then render(), which then tells each of the agents
  it has.

Updated 2019-03-17

'''
from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D
from world import World
from agent import Agent, AGENT_MODES  # Agent with seek, arrive, flee and pursuit

from random import randrange

def on_mouse_press(x, y, button, modifiers):
    if button == 1:  # left
        world.target = Vector2D(x, y)


def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        world.paused = not world.paused
    #------------------------------------
    elif symbol == KEY.K:
        # add another agent to the world
        world.agents.append(Agent(world, mode="hunter"))
    #------------------------------------
    if symbol == KEY.C:
      world.environments.append(environment(randrange(0, world.cx), randrange(0, world.cy), 90))
    #------------------------------------
    elif symbol in AGENT_MODES:
        for agent in world.agents:
            agent.mode = AGENT_MODES[symbol]

    # TASK 14 shooting agent
    # grenade
    if symbol == KEY.Z:
        for agent in world.agents:
            if agent.mode == 'hunter':
                agent.bullet_speed = 10
                agent.accurate = False
                agent.b_adjust = agent.bullet_speed * 2 
                print("grenade")
    # rocket
    if symbol == KEY.X:
        for agent in world.agents:
            if agent.mode == 'hunter':
                agent.bullet_speed = 10
                agent.accurate = True
                agent.b_adjust = agent.bullet_speed * 2 
                print("rocket")
    # hand gun
    if symbol == KEY.A:
        for agent in world.agents:
            if agent.mode == 'hunter':
                agent.bullet_speed = 20
                agent.accurate = False
                agent.b_adjust = agent.bullet_speed * 2 
                print("hand gun")
    # rifle
    if symbol == KEY.S:
        for agent in  world.agents:
            if agent.mode == 'hunter':
                agent.bullet_speed = 20
                agent.accurate = True
                agent.b_adjust = agent.bullet_speed * 2
                print("rifle")
    
    # increase rate of fire
    if symbol == KEY.R:
      for agent in world.agents:
        if agent.mode == 'hunter':
          agent.rate_of_fire += 1
    # decrease rate of fire
    if symbol == KEY.Q:
      for agent in world.agents:
        agent.rate_of_fire -= 1

def on_resize(cx, cy):
    world.cx = cx
    world.cy = cy


if __name__ == '__main__':

    # create a pyglet window and set glOptions
    win = window.Window(width=1000, height=1000, vsync=True, resizable=True)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # needed so that egi knows where to draw
    egi.InitWithPyglet(win)
    # prep the fps display
    fps_display = window.FPSDisplay(win)
    # register key and mouse event handlers
    win.push_handlers(on_key_press)
    win.push_handlers(on_mouse_press)
    win.push_handlers(on_resize)

    # create a world for agents
    world = World(500, 500)
    # add one agent
    world.agents.append(Agent(world))
    world.hunter = world.agents[0]
    world.hunter.hunted = True
    
    # unpause the world ready for movement
    world.paused = False

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render()
        fps_display.draw()
        # swap the double buffer
        win.flip()

