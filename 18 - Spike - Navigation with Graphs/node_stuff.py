# node_stuff.py

import pygame
from colours import WHITE, BLACK, BLUE, BROWN, RED, GREEN

terrain = {
  's': 'start',
  'g': 'goal',
  'c': 'clear',
  'x': 'walls',
  'w': 'water',
  'm': 'mud'
}

costs = {
  'start': 0,
  'goal': 0,
  'clear': 1,
  'mud': 3,
  'water': 5,
  'walls': None 
}

colours = {
  0: GREEN,
  1: WHITE,
  3: BROWN,
  5: BLUE, 
  None: BLACK
}
 
# takes in a list of nodes and appends adjacent nodes to the children field of each node
def populate_node_children(nodes):
  for node in nodes:
    node.children = get_adjacent_nodes(node, nodes)

  return nodes

# this is the worst function I have ever written bro
def get_adjacent_nodes(current, nodes):
  ret = []
  for node in nodes:
    if (node.x == current.x) and (node.y == current.y - node.height):
      ret.append(node)
  for node in nodes:
    if (node.x == current.x - node.width) and (node.y == current.y):
      ret.append(node)
  for node in nodes:
    if (node.x == current.x) and (node.y == current.y + node.height):
      ret.append(node)
  for node in nodes:
    if (node.x == current.x + node.width) and (node.y == current.y):
      ret.append(node)

  return ret

class node:
  def __init__(self, x, y, screen, width, height, terrain):
    self.x = x
    self.y = y
    self.screen = screen
    self.width = width
    self.height = height
    # cost variables
    self.terrain = terrain
    self.cost = costs[terrain]
    self.colour = colours[self.cost]
    # dictionary of adjactent nodes to this one
    self.children = [] 

  def draw(self):
    rect = pygame.Rect(self.x, self.y, self.width, self.height)
    pygame.draw.rect(self.screen, self.colour, rect)

  def update_values(self, terrain):
    self.terrain = terrain
    self.cost = costs[terrain]
    self.colour = colours[self.cost]

def randTerrain():
  return choice(list(costs.keys()))

def getTerrain(Map):
  return terrain[Map.pop()] 

def get_grid(screen, world_xy, screen_xy, Map):
  ret = []
  xy_length = [screen_xy[i] // world_xy[i] for i in range(len(screen_xy))]
  for y in range(world_xy[1]):
    for x in range(world_xy[0]):
      ret.append(node(x*xy_length[0], y*xy_length[1], screen, xy_length[0], xy_length[1], getTerrain(Map)))
  
  ret = populate_node_children(ret)
  return ret
      
