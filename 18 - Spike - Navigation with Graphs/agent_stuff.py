# agent_stuff.py
from priorityqueue import PriorityQueue
import pygame
from colours import RED

class Agent:
  def __init__(self, pos, goal, screen, nodes, mode='prey', air=False, prey=None):
    self.x = pos.x + 25 # hard coding is bad 
    self.y = pos.y + 25 # this is not great lol
    self.start_node = pos 
    self.screen = screen
    self.nodes = nodes

    self.goal = goal

    # INFO THAT AFFECTS AGENT TYPES 
    # if prey, goal is goal node
    self.mode = mode
    if self.mode == 'prey':
      self.path = self.a_star()
      self.speed = 1 # slow boi
      self.killed = False
    # if hunter, goal is a prey 
    if self.mode == 'hunter':
      self.speed = 2
      self.path = self.a_star()
      self.prey = prey

    # get the path from the a_star algorithm
    self.current_node = self.path.pop()

  def update(self):
    if self.mode == 'hunter':
      # update goal
      self.goal = self.prey.get_current_node()
      # update path
      self.path = self.a_star()
      # check if killed prey
      if self.current_node == self.prey.current_node:
        self.prey.killed = True

    self.move()
    self.draw()

  def draw(self):
    pygame.draw.circle(self.screen, RED, (self.x, self.y), 13) 
  
  def get_current_node(self):
    for node in self.nodes:
      if (node.x + 25, node.y + 25) == self.current_node:
        return node
    
  # moves the agent, called from update()
  def move(self):
    # if the agent is at its current node
    if (self.x, self.y) == self.current_node:
      if self.path:
        # pop the next node from the path
        self.current_node = self.path.pop()
    # else, add 1 to x or y
    elif self.x != self.current_node[0]:
      if self.x < self.current_node[0]:
        self.x += self.speed # TODO: give agent a speed value in place of this 1
      elif self.x > self.current_node[0]:
        self.x -= self.speed 
    elif self.y != self.current_node[1]:
      if self.y < self.current_node[1]:
        self.y += self.speed # TODO: give agent a speed value in place of this 1
      elif self.y > self.current_node[1]:
        self.y -= self.speed 

  def get_next_nodes(self, current):
    return current.children # this should work bro
	
  # should return a list of tuples in the right order to follow
  def backtrack(self, start, came_from, current):
    ret = []
    while (current.x, current.y) != (start.x, start.y):
      ret.append((current.x + 25, current.y + 25)) #add 25 to centre
      current = came_from[current]

    return ret
      
  # returns a list of tuples (x, y)
  def a_star(self):
    #start = self.nodes[0] # 0, 0, hard_coding is bad bro
    #start = self.start_node   
    if hasattr(self, 'current_node'):  #self.current_node:
      start = self.get_current_node() 
    else:
      start = self.start_node
    goal = self.goal 
    queue = PriorityQueue()
    queue.insert(start, 0)
    came_from = {}
    came_from[start] = None

    cost_so_far = {}
    cost_so_far[start] = 0 # no cost so far
  
    c = 0
    while queue: # can probs be just while queue: bro
      c += 1
      current = queue.get()
      next_nodes = self.get_next_nodes(current) 

      if (current.x, current.y) == (goal.x, goal.y):
        # do goal things bro
        return self.backtrack(start, came_from, current)

      for n in next_nodes:
        # we cannot move into a wall bro
        if n.terrain == 'walls':
          continue

        # not sure if this is right bro
        new_cost = cost_so_far[current] + current.cost
        
        if n not in cost_so_far or new_cost < cost_so_far[n]:
          cost_so_far[n] = new_cost
          priority = new_cost + heuristic((goal.x, goal.y), (n.x, n.y))
          queue.insert(n, priority)
          came_from[n] = current


  

def heuristic(a, b):
  (x1, y1) = a
  (x2, y2) = b
  return abs(x1 - x2) + abs(y1 - y2)

