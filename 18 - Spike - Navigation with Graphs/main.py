#!/usr/bin/env python3
# main.py

# TODO:
"""
5. re-factor and re-work agent so that it can be different types
6. allow the user to change the goal by clicking on a node
7. allow the user to add different types of agents to the game

AGENT TYPES:
prey:ground - moves to goal node, is affected by terrain costs
prey:air - moves to goal node, is not affected by terrain costs
hunder:ground - moves to agent, is affected by terrrain costs
hunter:air - moves to agent, is not affected by terrain costs

8. make different speeds available
"""

import pygame
from pygame.locals import *
from sys import argv
from random import choice
from agent_stuff import Agent
from node_stuff import get_grid, terrain, costs, colours 
from seventh_day import world
from colours import * 

def read_map(filename):
  text_file = open(filename)
  display = list(text_file.readline().split(" "))
  display = tuple([int(i) for i in display])
  grid_no = list(text_file.readline().split())
  grid_no = tuple([int(i) for i in grid_no])
  grid = []
  for i in range(grid_no[1]): # i for the num of rows (1 is the y grid num)
    for j in text_file.readline().rstrip("\n").split(" "):
      if j != '':
        grid.append(j)
 
  return display, grid_no, grid

def get_agent_start(nodes):
  ret_s = nodes[0] 
  ret_e = nodes[1]
  for node in nodes:
    if node.terrain == 'start':
      ret_s = node 
    elif node.terrain == 'goal':
      ret_e = node
  
  return ret_s, ret_e

def find_prey(world, goal):
  node = goal
  prey = None
  for a in world.agents:
    if a.mode == 'prey':
      node = a.get_current_node()
      prey = a

  return node, prey 

def get_user_input(event, world, screen, nodes):
  # H key: adds a ground hunter to the game
  if event.key == pygame.K_h:
    print("pressed tha h key")
    a_start, a_end = get_agent_start(nodes)
    a_end, p = find_prey(world, a_end)
    a = Agent(a_start, a_end, screen, nodes, mode='hunter', prey=p)
    world.agents.append(a)# hunter
  # L key: adds a ground prey to the game (when the prey dies, a new one 
  # will be added automatically)
  elif event.key == pygame.K_l:
    a_start, a_end = get_agent_start(nodes)
    a = Agent(a_start, a_end, screen, nodes)
    world.agents.append(a)
    print("added a ground prey to the game")

def get_node(pos, nodes):
  x, y = int(pos[0]), int(pos[1])
  for node in nodes:
    x1, y1 = node.x, node.y
    x2, y2 = x1 + node.width, y1 + node.height
    if x > x1 and x < x2 and y > y1 and y < y2:
      return node
  return nodes[0]
      
def get_mouse_input(event, world, screen, nodes):
  pos = pygame.mouse.get_pos()
  new_goal = get_node(pos, world.nodes)
  #for node in world.nodes:
    #if node.terrain == 'goal' and node != new_goal:
      #node.update_values('clear') 

  new_goal.update_values('goal')
  # make the agents search again
  for agent in world.agents:
    if agent.mode == 'prey':
      agent.goal = new_goal
      agent.path = agent.a_star()

def main():
  # command line arguments
  # read map from file
  if len(argv) > 1:
    display, grid_no, grid = read_map(argv[1])
  else:
    display, grid_no, grid = read_map("map1.txt")
  
  #init
  pygame.init()

  background = BLACK 
  screen = pygame.display.set_mode(display)
  # create nodes (we need to do this here so agents can have this data)
  grid.reverse()
  # get nodes (with node children)
  nodes = get_grid(screen, grid_no, display, grid) # hard coding is bad
  # create agent
  a_start, a_end = get_agent_start(nodes)
  a = Agent(a_start, a_end, screen, nodes)
  agents = [a]
  # create the world (like a god)
  w = world(screen, background, agents, nodes)
  running = True
  while running:
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        pygame.quit()
        running = False
      elif event.type == pygame.locals.KEYDOWN:
        # get user input
        get_user_input(event, w, screen, nodes)
      elif event.type == pygame.locals.MOUSEBUTTONUP:
        get_mouse_input(event, w, screen, nodes)
    # update world
    w.update()
    # not sure if I should have this here or in world.update()
    pygame.display.update()

if __name__ == "__main__":
  main()
