# priorityqueue.py

class PriorityQueue:
  def __init__(self):
    self.contents = []

  def insert(self, item, priority):
    self.contents.append((priority, item))

  def get(self):
    ret = max(self.contents, key=lambda x: x[0])
    self.contents.remove(ret)
    return ret[1]

  def empty(self):
    self.contents.clear()
