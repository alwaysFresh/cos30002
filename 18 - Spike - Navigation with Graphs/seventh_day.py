# seventh_day.py
import pygame

# a grid of x times y squares 
class world:
  def __init__(self, screen, background, agents, nodes):
    # pygame display
    self.screen = screen 
    # background colour
    self.background = background
    # caption
    pygame.display.set_caption("DO YOU EVEN SPIKE BRO")
    # need to google what this does
    pygame.display.flip()
    # our grid
    self.block_size = 50
    # our agents
    self.agents = agents
    # our nodes
    self.nodes = nodes 
    
  def update(self):
    # render world
    self.draw()
    # update agents
    for a in self.agents:
      if a.mode == 'prey' and a.killed:
        self.agents.remove(a)

      a.update()

  def draw(self):
    self.screen.fill(self.background)

    for node in self.nodes:
      node.draw()


