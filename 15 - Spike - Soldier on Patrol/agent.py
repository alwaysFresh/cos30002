'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians, asin, sqrt
from random import random, randrange, uniform # half of this stuff has already been done
from path import Path
from bullet import bullet
import pyglet

AGENT_MODES = {
    KEY._1: 'attack',
    KEY._2: 'patrol',
}

class Agent(object):

    def __init__(self, world=None, scale=30.0, mass=1.0, mode='prey'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass

        # data for drawing this agent
        self.color = 'ORANGE'
        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

        # path details
        self.path = Path()
        self.randomise_path()
        self.waypoint_threshold = 30.5

        ### wander details
        self.wander_target = Vector2D(1, 0)
        self.wander_dist = 1.0 * scale
        self.wander_radius = 1.0 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale

        # limits?
        self.max_speed = 5.0 * scale
        self.max_force = 500.0

        # TASK 15 stuff
        self.neigborhood = 200
        self.health = 3

        # TASK 14 stuff
        if self.mode == 'patrol':
            self.accurate = True 
            self.bullet_speed = 20
            self.b_radius = 5
            self.b_adjust = self.bullet_speed * 2
            #self.max_force = 0.0
            self.pos = Vector2D(500, 500)
            self.bullets = []
            self.rate_of_fire = 1
            self.time_to_shoot = 50

            # TASK 15 stuff
            self.soldier_state = "patrol"
            self.fighting_state = 'shooting'
            self.ammo = 50
             
        if self.mode == 'prey':
            self.max_speed = 10.0 * scale
            self.max_force = 300.0
            self.color = 'PINK'
            self.vehicle_shape = [
              Point2D(-0.5, 0.3),
              Point2D(0.5, 0.0),
              Point2D(-0.5, -0.3)       
            ]
                  # debug draw info?
        self.show_info = False

    def calculate(self, delta):
            # calculate the current steering force
        mode = self.mode
        # prey mode, will try to flee from the hunter and hide behind circles
        if mode == 'prey':
            force = self.prey(delta)
            self.bullets = []

        elif mode == 'patrol':
            #force = self.hunter(delta)
            force = self.new_soldier()
        else:
            force = Vector2D()

        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        ## force = self.calculate()
        force = self.calculate(delta)  # <-- delta needed for wander | thannkyou!
        force.truncate(self.max_force)
        ## limit force? <-- for wander
        # ...
        # TASK 14 
        # this code runs slower than I do (I hurt my knee)
        if self.mode == 'patrol':
            if self.time_to_shoot < 50:
                self.time_to_shoot += self.rate_of_fire 
            for bullet in self.bullets:
                bullet.update()
                for agent in self.world.agents:
                    if agent.mode == 'prey':
                        if bullet.pos.distance(agent.pos) < self.b_radius * 4:
                            agent.health -= 1
                            self.bullets.remove(bullet)
                            print("hit")
                            if agent.health <= 0:
                                self.world.agents.remove(agent)
            
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        
        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

        #render path
        if self.mode == 'patrol':
            # render neighborhood
            egi.circle(self.pos, self.neigborhood)
            self.path.render()
            if self.bullets:
                for bullet in self.bullets:
                    bullet.render()
            
            #print the state of our soldier
            text =  'state: ' + self.soldier_state + ' attack_state: ' + self.fighting_state
            label = pyglet.text.Label(text, font_name='Times New Roman', font_size=36, x=10, y=30)
            label.draw()


        if self.mode == 'prey':
            # render prey health
            hy = self.pos.y - 40
            hx = self.pos.x - 10 
            egi.rect(hx, hy, hx + (self.health * 20), hy + 10, filled=True)
        
        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

        


    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)


    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target

        jitter_tts = self.wander_jitter * delta
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius
        target = wt + Vector2D(self.wander_dist, 0)
        # it is terrible practice to have two separate scale values my bad (30 parameter)
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        return self.seek(wld_target)
    
    # accessing the prey_pos list comprehension by index is bad practice, not ideal
    def hunter(self, delta):
        target = Vector2D()
        for agent in self.world.agents:
            if agent.mode == 'prey':
                target = agent 
        if self.time_to_shoot >= 100:
            self.shoot(target)
            self.time_to_shoot = 0
        ret = Vector2D()
        return ret

    def prey(self, delta):
        return self.wander(delta)

    def shoot(self, target):
        # we need to adjust velocity to predict where the agent will be 
        pos = Vector2D(self.pos.x, self.pos.y) 

        vel = self.predictPos(target)
        # 20 fast, 10 slow
        self.bullets.append(bullet(pos, vel, self.bullet_speed, self.b_radius))  


    def predictPos(self, target):
        toTarget = target.pos - self.pos
        # make shot less acurate if needed
        if not self.accurate:
            toTarget += Vector2D(randrange(-300, 300), randrange(-300, 300))
        # changed max_speed to speed() no clue why I didn't do this in TASK14
        lookAheadTime = toTarget.length() / ((self.bullet_speed * self.b_adjust) + target.speed())
        lookAheadPos = target.pos + (target.vel * lookAheadTime)
        egi.line_by_pos(self.pos, lookAheadPos)
        #return self.mod_seek(lookAheadPos)
        return (lookAheadPos - self.pos).normalise() * self.max_speed

    def randomise_path(self):
        cx = self.world.cx
        cy = self.world.cy
        margin = min(cx, cy) * (1/6)
        pts = randrange(4, 8) 
        self.path.create_random_path(pts, 0, 0, cx, cy, True)

    def follow_path(self):
        curr_pt = self.path._pts[self.path._cur_pt_idx]
        
        if self.pos.distance(curr_pt) <= self.waypoint_threshold:
            self.path.inc_current_pt()
        ret = self.seek(curr_pt)

        return ret

    def arrive(self, target_pos):
        ''' this behaviour is similar to seek() but it attempts to arrive at
            the target position with a zero velocity'''
        decel_rate = 0.9  #self.DECELERATION_SPEEDS[speed]
        to_target = target_pos - self.pos
        dist = to_target.length()
        if dist > 0:
            # calculate the speed required to reach the target given the
            # desired deceleration rate
            speed = dist / decel_rate
            # make sure the velocity does not exceed the max
            speed = min(speed, self.max_speed)
            # from here proceed just like Seek except we don't need to
            # normalize the to_target vector because we have already gone to the
            # trouble of calculating its length for dist.
            desired_vel = to_target * (speed / dist)
            return (desired_vel - self.vel)
        return Vector2D(0, 0)

    def update_soldier(self):
        ret = 'patrol'
        target = None
        for agent in self.world.agents:
            if agent.mode == 'prey' and self.pos.distance(agent.pos) < self.neigborhood:
                ret = 'attack'
                target = agent

        return ret, target


    def new_soldier(self):
        ret = Vector2D()
        # two states
        self.soldier_state, target = self.update_soldier()
        # attack
        if self.soldier_state == 'attack':    
            # attack has two states
            self.max_force = 0.0 
            # reloading
            if self.fighting_state == 'reloading': 
                self.ammo += 1
                if self.ammo >= 50:
                    self.fighting_state = 'shooting'
            # shooting
            elif self.fighting_state == 'shooting':
                if self.time_to_shoot >= 50:
                    self.shoot(target)
                    self.ammo -= 10
                    self.time_to_shoot = 0
                    if self.ammo <= 0:
                        self.fighting_state = 'reloading'

        # patrol
        elif self.soldier_state == 'patrol':
            self.max_force = 500.0
            ret = self.follow_path()

        return ret
   
