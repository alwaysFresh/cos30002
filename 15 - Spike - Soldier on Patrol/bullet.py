# bullet.py

from graphics import egi, KEY

class bullet:
  def __init__(self, pos, vel, speed, rad): # position and velocity
    self.pos = pos
    self.vel = vel # i guess this is bullet speed
    self.max_speed = speed  
    self.rad = rad
  
  def render(self):
    egi.circle(self.pos, self.rad) # render the bullet 

  def update(self):
    self.vel.truncate(self.max_speed)
    self.pos += self.vel

