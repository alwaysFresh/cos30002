

class vulnerableBot(object):
  def __init__(self):
    self.saved_state = None
  
  def update(self, gameinfo):
    
    # only send one fleet at a time
    if gameinfo.my_fleets:
      return
    
    if gameinfo.my_planets and gameinfo.not_my_planets:
      target = get_target(gameinfo.not_my_planets, self.saved_state)
      src = max(gameinfo.my_planets.values(), key = lambda x: x.num_ships)

      # we sent 80% and don't check for a min value because we like to live life dangerously
      if src.num_ships > 10:
        self.saved_state = gameinfo.not_my_planets
        gameinfo.planet_order(src, target, int(src.num_ships * 0.80))
      
              
      


def get_target(enemyplanet, saved_state = None):
  result = None

  if saved_state is None:
    result = max(enemyplanet.values(), key = lambda x: x.num_ships)

    return result
  for old, curr in zip(saved_state.values(), enemyplanet.values()):
    print(curr.num_ships, old.num_ships)
    if curr.num_ships < old.num_ships:
      result = curr
  if result is None:
    print("this bot is a fucking failure")
    result = max(enemyplanet.values(), key = lambda x: x.num_ships) 

  return result

    


    
      
      
    


"""

plan 

compare the current state of the enemy's planets, if a planets fleet is smaller than it was last
move, that likely means that it sent its fleet (most of the bots in this directory send 75% of their fleet)
leaving it vulnerable, if we sent a high percentage of the largest planet fleet to this vulnerable planet,
this might be effective, however I do not know if it would be more effective than just sending the largest fleet  
to the smallest enemy planet without checking for changes and just attacking the lowest value 
"""


