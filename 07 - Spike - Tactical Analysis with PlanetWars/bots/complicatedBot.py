# complicatedBot.py
"""
I am not very good at strategy so the strategies themselves are not 
super complicated, 
if the bot is winning, it attacks the most powerful planet it does not own
if the bot is losing, it attacks the least powerful planet it does not own
if the bot is neither winning or losing, it attacks the median size planet it does not own

theoretically the strategies could be switched out for more effective/ suitable ones, and 
we could measure winning in terms of the sum of the enemies ships just as easily as the total 
number of planets and it would be pretty easy to implement 

"""

from statistics import median as med

class complicatedBot(object):
  def update(self, gameinfo):
    # we will use something that resembles a finite state machine
    # unfortunately I can only think of 2 states at this moment so that will have to do

    if gameinfo.my_fleets:
      return

    if gameinfo.my_planets and gameinfo.not_my_planets:
      src = max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)    
      winning = self.winning(gameinfo.my_planets, gameinfo.enemy_planets)

      # if winning 
      if winning: 
        # attack the enemy's or neutral most powerful planets 
        dest = max(gameinfo.not_my_planets.values(), key=lambda p: p.num_ships)
        print("i am winning")
      # else if not winning 
      elif winning is False:
        # attack the least powerful planet
        dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.num_ships)
        print("i am losing")
      # else if the game is roughly even ( range of 4 from completely even )
      elif winning is None: # the game is even, lets say within a range of 4 planets 
        # attack the enemy's or(neutral) median size planet ( in terms of ship count) 
        median = med(map(lambda p: p.num_ships, gameinfo.not_my_planets.values()))

        #for i in gameinfo.not_my_planets.values():
          #if i.num_ships == int(median):
            #dest = i

        dest = self.closest(gameinfo.not_my_planets, median)
        print("I am not winning or losing")

      if src.num_ships > 5:
        gameinfo.planet_order(src, dest, int(src.num_ships * 75))
  
  def closest(self, arr, value):
    # not gonna lie, I am pretty proud of this one
    # adapted it from a geeksforgeeks tutorial, changed it enough to be proud of it though
    return min(arr.values(), key=lambda i: abs(i.num_ships - value))

  def winning(self, my_ships, enemy_ships):
    ret = False
    my_sum = sum(my_ships)
    their_sum = sum(enemy_ships)
    diff = (my_sum - their_sum)
    if abs(diff) in range(4):
      # if we return None, the else statement should be called
      # which means neither side is winning
      ret = None
    elif my_sum > their_sum:
      ret = True

    return ret
  
  
