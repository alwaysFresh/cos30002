
class tacticalBot(object):
  def update(self, gameinfo):
    
    # if the bot has more planets than the other bot

    # reinforce our plantets instead of attacking the other plabnets 

    if gameinfo.my_fleets:
      return
    
    if sum(gameinfo.my_planets) > sum(gameinfo.not_my_planets) and gameinfo.my_planets and gameinfo.not_my_planets:
      print("tact bot thinks its winning")
      dest = min(gameinfo.my_planets.values(), key=lambda p: p.num_ships)

      src = max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)

      if src.num_ships > 5:
        gameinfo.planet_order(src, dest, int(src.num_ships * 0.75))

    elif sum(gameinfo.my_planets) < sum(gameinfo.not_my_planets):
      print("tact bot thinks its losing")
      dest = min(gameinfo.not_my_planets.values(), key=lambda p: p.num_ships)

      src = max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)

      if src.num_ships > 5:
        gameinfo.planet_order(src, dest, int(src.num_ships * 0.75))
