#!/usr/bin/env python3 
import time
from sys import argv

# all the goals have been completed, its a loss!!!

class action:
  def __init__(self, name, goals, goal_change, costs, cost_change):
    self.name = name
    self.goals = goals # list of goals impacted by the action
    self.goal_change = goal_change
    # implementing a cost system
    self.costs = costs
    self.cost_change = cost_change

  def get_goal_change(self, goal):
    return self.goal_change[goal]

  def get_cost_change(self, cost):
    return self.cost_change[cost]

goals = {
  "defeat_enemies": 3, # essentially the enemy health
  "stay_alive"    : 2 # essentially our health (the agent)
}

actions = {
  "drink_potion": action("drink_potion", ["stay_alive"], [-3], ["money"], [-4]),
  "shoot_enemy": action("shoot_enemy", ["defeat_enemies", "stay_alive"], [-3, 1], ["money"], [-1]),
  "kiss_wound": action("kiss_wound", ["stay_alive"], [-1], ["money"], [-1])
}

# we have a universal cost for shooty and potion so it has to choose between them
costs = {
  "money": 4
}

def get_discontent(action, c_goals=goals):
  temp_goals = dict(c_goals)

  for g_index, goal in enumerate(action.goals):
    temp_goals[goal] += action.get_goal_change(g_index)
    # check to not go past 0
    if temp_goals[goal] < 0:
      temp_goals[goal] = 0
  
  discontent = sum([i*i for i in temp_goals.values()])
  
  print("\t -", action.name, "discontent", discontent)

  return discontent

def update(goap):
  print("\n----------------------------\n")
  print("GOALZ", goals)
  print("COSTZ", costs)

  if goap:
    best_action = choose_action_dfs()
  else:
    best_action = choose_action()

  print("BEST ACTION", best_action.name)

  for g_index, goal in enumerate(best_action.goals):
    # apply action to goal
    goals[goal] = goals[goal] + best_action.get_goal_change(g_index)
  for c_index, cost in enumerate(best_action.costs):
    costs[cost] = costs[cost] + best_action.get_cost_change(c_index)
  
  print("UPDATED GOALZ", goals)

def run_until_all_goals_zero(goap):
  while sum(goals.values()) > 0:
    if sum(costs.values()) <= 0:
        print("I ran out of money", costs)
        break
    update(goap)
    
    time.sleep(1)

def is_valid(action, c_costs=costs):
  ret = True
  for c_index, cost in enumerate(action.costs):
    # if we have enough money
    if abs(action.get_cost_change(c_index)) > c_costs[cost]:
      ret = False
  # if all of the associated goals have already been completed
  goal_num = len(action.goals) 
  completed_goal_num = sum([1 for goal in action.goals if goals[goal] <= 0])
  if goal_num == completed_goal_num:
    ret = False
    
  return ret

def plan_ahead(cur_depth, limit, goal_cpy, cost_cpy, discontent, min_discontent):
  # print current info
  actions_list = [a for a in actions.keys() if is_valid(actions[a], c_costs=cost_cpy)]
  for action in actions_list:
    # if we are not in deep enough, go deeper
    if cur_depth == limit:
      cur_depth -= 1
      return min_discontent

    else:

      print(" " * cur_depth, "DEPTH", cur_depth) 
      discontent = get_discontent(actions[action], c_goals=goal_cpy)

      # edit info and recurse bro
      new_cur_depth = int(cur_depth + 1)
      
      new_goals, new_cost = dfs_make_changes(action, goal_cpy, cost_cpy)

      if min_discontent > discontent:
        min_discontent = discontent

      plan_ahead(new_cur_depth, limit, new_goals, new_cost, discontent, min_discontent)

  return min_discontent

# parameters: action, goals, costs 
def dfs_make_changes(action, c_goals, c_costs):
  new_goals = dict(c_goals)
  new_costs = dict(c_costs)

  for g_index, g in enumerate(actions[action].goals):
    new_goals[g] += actions[action].get_goal_change(g_index)
  for c_index, c in enumerate(actions[action].costs):
    new_costs[c] += actions[action].get_cost_change(c_index)

  return new_goals, new_costs

def choose_action_dfs():
  goal_cpy = dict(goals)
  cost_cpy = dict(costs)
  base = sum([i*i for i in goal_cpy.values()])
  print("BASE", base)
  actions_list = [a for a in actions.keys() if is_valid(actions[a])]

  discontents = []
  
  # initialise min discontent 
  min_discontent = base
  # depth and depth limit
  for action in actions_list:
    print("DEPTH", 0)
    discontent = get_discontent(actions[action], c_goals=goal_cpy)
    if discontent < min_discontent:
      min_discontent = discontent

    new_goals, new_cost = dfs_make_changes(action, goal_cpy, cost_cpy)
      
    discontents.append(plan_ahead(1, 2, new_goals, new_cost, discontent, min_discontent))
  
  return actions[actions_list[discontents.index(min(discontents))]]

def choose_action():
  # construct the actions list of actions we can afford 
  actions_list = [a for a in actions.keys() if is_valid(actions[a])]
  print("ACTIONS WE CAN AFFORD")

  # first in list is default
  best_action = actions_list[0]
  best_value = get_discontent(actions[best_action])
  
  # loop through remaining actions and compare
  for action in actions_list[1:]:
    this_value = get_discontent(actions[action])
    if this_value < best_value:
      best_value = this_value
      best_action = action

  return actions[best_action]

def main():
  file, goap_arg = argv
  goap = False
  if goap_arg == "goap":
    goap = True
  run_until_all_goals_zero(goap)

if __name__ == "__main__":
  main()
