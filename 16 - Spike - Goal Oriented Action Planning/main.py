#!/usr/bin/env python3

import time
import copy

class PriorityQueue:
  def __init__(self):
    self.contents = []

  def insert(self, item, priority):
    self.contents.append((priority, item))

  def get(self):
    ret = max(self.contents, key=lambda x: x[0])
    return ret

  def empty(self):
    self.contents.clear()

  def print_contents(self):
    for c in self.contents:
      print("item:", c[1], "priority:", c[0])

# a world is a list, eg[0, 1, 0] where the 1's are enemies
class Agent:
  def __init__(self, world, health=1, ammo=1, potions=1):
    self.pos = 0 # might make this dynamic, or len(world) // 2 later 
    self.world = world
    self.health = health
    self.ammo = ammo
    self.potions = potions
    self.goals = PriorityQueue()
    self.max_health = 6 # hard coding is bad

    self.evaluate_goals()

  # TODO: finish this stuff, add a run_until_all_goals_zero method which is just a while update() 
  # thing bro
  
  def run_until_no_goals(self):
    while self.not_done():
      print("\n-------------------------------\n")
      w = self.world[:]
      w[self.pos] = 'x'
      print("world", w)
      print("health", self.health, " ammo", self.ammo, " potions", self.potions)
      self.update()
      self.goals.print_contents()
      time.sleep(1)

  def not_done(self):
    return sum([i[0] for i in self.goals.contents]) > 0

  def evaluate_goals(self):
    # this will not work for larger worlds (will overly prioritised)
    attack_priority = sum([i for i in self.world if i > 0])
    # if our health is low, the priority will be high 
    health_priority = self.max_health - self.health
    self.goals.empty()
    self.goals.insert("attack", attack_priority)
    self.goals.insert("health", health_priority)

  def update(self):
    ## UPDATE PRIORITIES 
    self.evaluate_goals() 

    current_goal = self.goals.get()

    self.apply_action(current_goal[1]) # decide which action to take based on the goal 

  # TODO:
  # make the agent check the world ahead of it, to decide whether it may need ammo or potions later
  # and factor this into the decision of what action to take

  def apply_action(self, goal):
    if goal == "health":
      print(" -- need healing -- ")
      # if we don't have potions, or using a potion would waste its potential
      if self.potions <= 0 or self.max_health - self.health < 3:
        self.flee() # run away and recover
      else:
        self.drink_potion()

    elif goal == "attack":
      print(" --i AM ATTACKING--")
      # we will make this its own thing later
      if self.world[self.pos] <= 0:
        self.move_forward()
      # if we have ammo and the bullet is neccessary to defeat the enemy
      elif self.ammo > 0 and self.world[self.pos] > 0.5: 
        self.shoot_enemy()
      else:
        self.punch_enemy()
    else:
      print("agent doesnt have any actions for", goal)
  
  # TODO: make a "jetpack" that can move the agent to wherever the next enemy is

  def move_forward(self):
    print("-- i step forward -- ")
    if self.pos < len(self.world) - 1:
      self.pos += 1
  # we don't need a move backward, we can use flee

  # if player has ammo and priority is goal=defeat enemies
  def shoot_enemy(self):
    print("-- i shoot enemy -- ")
    self.ammo -= 1 # we will assume we have ammo
    if self.world[self.pos] > 0:
      # takes damage from the enemy
      self.health -= self.world[self.pos] # we will make the damage taken equal to the damage dealt
      self.world[self.pos] -= 1

  # if player has no ammo and priority is goal=defeat enemies
  def punch_enemy(self):
    print(" -- i punch enemy --")
    if self.world[self.pos] > 0: # if there is an enemy at this location
      self.world[self.pos] -= 0.5 # punching does less damage

  # if player has a potion and priority is goal=stay alive
  def drink_potion(self):  
    print(" -- I drink potion -- ")
    if self.potions >= 1: # if we have at least 1 potion 
      self.potions -= 1
      self.health += 3
      while self.health > self.max_health:
        self.health -= 1
        print(self.health, "health went over the max bro")

  # if player has no potions and priority is goal=stay alive
  def flee(self):
    print(" -- I flee --")
    if self.pos > 0: # we cannot flee from 0
      self.pos -= 1
      if self.health < self.max_health:
        self.health += 1 # less effective than a potion

def main():
  world = [0, 1, 1, 0]
  # world, health ammo, potions
  big_boi = Agent(world, health=6, ammo = 2, potions = 1)
  big_boi.run_until_no_goals()


if __name__ == '__main__':
  main()
