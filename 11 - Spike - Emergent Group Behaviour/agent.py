'''An agent with Seek, Flee, Arrive, Pursuit behaviours
vel
Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform # half of this stuff has already been done
from path import Path
from statistics import mean # TASK 11: we need to average the vectors for cohesion or something 

AGENT_MODES = {
    KEY._1: 'seek',
    KEY._2: 'wander',
    KEY._3: 'cohesion',
    KEY._4: 'separation',
    KEY._5: 'alignment'
}

class Agent(object):

    def __init__(self, world=None, scale=30.0, mass=1.0, mode='wander', spaceinfo=False):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass

        # data for drawing this agent
        self.color = 'ORANGE'
        self.vehicle_shape = [
            Point2D(-0.5,  0.3),
            Point2D( 0.5,  0.0),
            Point2D(-0.5, -0.3)
        ]
        
        ### wander details
        self.wander_target = Vector2D(1, 0)
        self.wander_dist = 1.5 * scale
        self.wander_radius = 0.5 * scale
        self.wander_jitter = 10.0 * scale
        self.bRadius = scale

        # limits?
        self.max_speed = 10.0 * scale
        self.max_force = 200.0

        # debug draw info?
        self.show_info = False
        
        # TASK 11: parameters
        self.tElevenInfo = spaceinfo
        self.neighborhood = 150 # radius of neigborhood
        self.personal_space = 75 # radius of personal space
        # weighting for forces
        self.w_weight = 1.0
        self.c_weight = 1.0
        self.s_weight = 1.0
        self.a_weight = 1.0

    def calculate(self, delta):
        # calculate the current steering force
        mode = self.mode
        if mode == 'seek':
            force = self.seek(self.world.target)
        elif mode == 'wander':
            force = self.egb(delta)
        else:
            force = Vector2D()
        self.force = force
        return force

    def update(self, delta):
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        ## force = self.calculate()
        force = self.calculate(delta)  # <-- delta needed for wander | thannkyou!
        force.truncate(self.max_force)
        ## limit force? <-- for wander
        # ...
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,
                                          self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

        # draw wander info?
        if self.tElevenInfo:
            # calculate the center of the wander circle in front of the agent
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            # draw the wander circle
            egi.green_pen()
            egi.circle(wld_pos, self.wander_radius)
            # draw the wander target (little circle on the big circle
            egi.red_pen()
            wnd_pos = (self.wander_target +  Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            egi.circle(wld_pos, 3)

        # add some handy debug drawing info lines - force and velocity
        if self.show_info:
            s = 0.5 # <-- scaling factor
            # force
            egi.red_pen()
            egi.line_with_arrow(self.pos, self.pos + self.force * s, 5)
            # velocity
            egi.grey_pen()
            egi.line_with_arrow(self.pos, self.pos + self.vel * s, 5)
            # net (desired) change
            egi.white_pen()
            egi.line_with_arrow(self.pos+self.vel * s, self.pos+ (self.force+self.vel) * s, 5)
            egi.line_with_arrow(self.pos, self.pos+ (self.force+self.vel) * s, 5)

        # TASK 11: rendering shenanigans
        if self.tElevenInfo:
            egi.circle(self.pos, self.neighborhood)
            egi.circle(self.pos, self.personal_space)


    def speed(self):
        return self.vel.length()

    #--------------------------------------------------------------------------

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def flee(self, hunter_pos):
        ''' move away from hunter position '''
        ## add panic distance (second)
        if (self.pos.distance(hunter_pos)) < 300:
            desired_vel = (self.pos - hunter_pos).normalise() * self.max_speed
            return (desired_vel - self.vel) 
        return Vector2D()
    
    def wander(self, delta):
        ''' Random wandering using a projected jitter circle. '''
        wt = self.wander_target

        jitter_tts = self.wander_jitter * delta
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        wt.normalise()
        wt *= self.wander_radius
        target = wt + Vector2D(self.wander_dist, 0)
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        return self.seek(wld_target)
    
    #TODO: something is wrong with with cohesion, the agents tend to follo whatever the first vector is 
    def cohesion(self):
        # average the seek vector of every neighbor
        neighbors = []
        for agent in self.world.agents:
            if agent != self:
                distance = self.pos.distance(agent.pos)
                #if self.pos.distance(agent.pos) < self.neighborhood:
                if distance < self.neighborhood and distance > self.personal_space:
                    neighbors.append(agent.pos)

        if neighbors != []:
            ret = Vector2D(mean([n.x for n in neighbors]), mean([n.y for n in neighbors]))
            ret = self.seek(ret)
        else:
            ret = None
        return ret

    def separation(self):
        # average the seek vector of every neighbor
        neighbors = []
        for agent in self.world.agents:
            if agent != self:
                distance = self.pos.distance(agent.pos)
                if distance <= self.personal_space:
                    neighbors.append(agent.pos)

        if neighbors != []:
            ret = Vector2D(mean([n.x for n in neighbors]), mean([n.y for n in neighbors]))
            ret = self.flee(ret)
        else:
            ret = None
        return ret

    '''
    def alignment(self):
        # average the seek vector of every neighbor
        neighbors = []
        #neighbors.append(Vector2D())
        for agent in self.world.agents:
            if agent != self:
                distance = self.pos.distance(agent.pos)
                if distance <= self.personal_space:
                    neighbors.append(agent.vel)
        
        if neighbors != []:
            ret = Vector2D(mean([n.x for n in neighbors]), mean([n.y for n in neighbors]))
            ret = self.seek(ret)
        else:
            ret = None
        return ret
    '''        
    def alignment(self):
        avDir = Vector2D()
        neighborNo = 0
        for agent in self.world.agents:
            distance = self.pos.distance(agent.pos)
            if distance < self.neighborhood:
                avDir = avDir + agent.vel
                neighborNo+= 1

        ret = avDir / neighborNo
        return ret
        
    def egb(self, delta):
        cohesion = self.cohesion()
        separation = self.separation()
        alignment = self.alignment()

        wander = self.wander(delta)

        if alignment is not None:
            wander += alignment * self.a_weight
        if cohesion is not None:
            wander += cohesion * self.c_weight
        if separation is not None:
            wander += separation * self.s_weight

        return wander
        
    '''
    # TASK 11: this is the method we will be calling boi
    def egb(self, delta):
        # we combine separation, cohesion, alignment and wander and return the combined vector
        cohesion = self.cohesion() * self.c_weight
        separation = self.separation() * self.s_weight
        alignment = self.alignment() * self.a_weight
        wander = self.wander(delta) * self.w_weight
        
        # add all the forces together and add weighting if you want dude
        ret = wander + cohesion + separation + alignment# + all the other ones we haven't defined yet
        return ret
        '''

"""
neighbors = []
neighbors.append(Vector2D(randrange(self.world.cx), randrange(self.world.cy)))
for agent in self.world.agents:
    if agent != self:
        distance = self.pos.distance(agent.pos)
        if distance < self.neighborhood and distance > self.personal_space:
            neighbors.append(agent.vel)

ret = Vector2D(mean([n.x for n in neighbors]), mean([n.y for n in neighbors]))
# do I seek or just return the vel?
return ret
"""
