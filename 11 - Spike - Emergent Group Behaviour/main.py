#!/usr/bin/env python3
'''Autonomous Agent Movement: Paths and Wandering

Created for COS30002 AI for Games by Clinton Woodward <cwoodward@swin.edu.au>

For class use only. Do not publically share or post this code without permission.

This code is essentially the same as the base for the previous steering lab
but with additional code to support this lab.

'''
from graphics import egi, KEY
from pyglet import window, clock
from pyglet.gl import *

from vector2d import Vector2D
from world import World
from agent import Agent, AGENT_MODES  # Agent with seek, arrive, flee and pursuit


def on_mouse_press(x, y, button, modifiers):
    if button == 1:  # left
        world.target = Vector2D(x, y)


def on_key_press(symbol, modifiers):
    if symbol == KEY.P:
        world.paused = not world.paused
    elif symbol == KEY.K:
        world.agents.append(Agent(world))
    elif symbol in AGENT_MODES:
        for agent in world.agents:
            agent.mode = AGENT_MODES[symbol]
    # the pythonic way???
    elif symbol == KEY.R:
        [a.randomise_path() for a in world.agents]
         
    # Toggle debug force line info on the agent
    elif symbol == KEY.I:
        for agent in world.agents:
            agent.show_info = not agent.show_info
    
    # TASK 11: keys to modify parameters that are important to me
    # personal space
    elif symbol == KEY.A:
        for agent in world.agents:
            agent.personal_space += 10
    elif symbol == KEY.Z:
        for agent in world.agents:
            agent.personal_space -= 10
    # neighborhood
    elif symbol == KEY.S:
        for agent in world.agents:
            agent.neighborhood += 10
    elif symbol == KEY.X:
        for agent in world.agents:
            agent.neighborhood -= 10
    # change weights
    elif symbol == KEY.D:
        for agent in world.agents:
            agent.w_weight += 10
            print(agent.w_weight)
    elif symbol == KEY.C:
        for agent in world.agents:
            agent.w_weight -= 10
            print(agent.w_weight)
    elif symbol == KEY.F:
        for agent in world.agents:
            agent.c_weight += 10
            print(agent.c_weight)
    elif symbol == KEY.V:
        for agent in world.agents:
            agent.c_weight -= 10
            print(agent.c_weight)
    elif symbol == KEY.G:
        for agent in world.agents:
            agent.s_weight += 10
            print(agent.s_weight)
    elif symbol == KEY.B:
        for agent in world.agents:
            agent.s_weight -= 10
            print(agent.s_weight)
    elif symbol == KEY.H:
        for agent in world.agents:
            agent.a_weight += 10
            print(agent.a_weight)
    elif symbol == KEY.N:
        for agent in world.agents:
            agent.a_weight -= 10
            print(agent.a_weight)


def on_resize(cx, cy):
    world.cx = cx
    world.cy = cy


if __name__ == '__main__':

    # create a pyglet window and set glOptions
    win = window.Window(width=1000, height=1000, vsync=True, resizable=True)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # needed so that egi knows where to draw
    egi.InitWithPyglet(win)
    # prep the fps display
    fps_display = window.FPSDisplay(win)
    # register key and mouse event handlers
    win.push_handlers(on_key_press)
    win.push_handlers(on_mouse_press)
    win.push_handlers(on_resize)

    # create a world for agents
    world = World(1000, 1000)
    # add one agent
    world.agents.append(Agent(world, spaceinfo=True))
    # unpause the world ready for movement
    world.paused = False

    while not win.has_exit:
        win.dispatch_events()
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        # show nice FPS bottom right (default)
        delta = clock.tick()
        world.update(delta)
        world.render()
        fps_display.draw()
        # swap the double buffer
        win.flip()

