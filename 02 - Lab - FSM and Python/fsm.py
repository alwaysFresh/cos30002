# Finite State Machine

import random

money = 0
hunger = 0

# existing states are poor, ok, hungry
current_state = 'poor'

running = True
max_limit = 100
game_time = 0

while running:
	game_time += 1
	if current_state == 'poor':
		print("please give me money thank you")
		money += random.randint(0,2)
		hunger += random.randint(0,2)
		if money > 4:
			current_state = 'ok'
	
	elif current_state == 'ok':
		print("I am ok")
		money -= random.randint(0,2)
		hunger += random.randint(0,2)
		if hunger > 5:
			current_state = 'hungry'
		if money < 1:
			current_state = 'poor'
	
	elif current_state == 'hungry':
		print("I need food")
		money -= random.randint(0,2)
		hunger += random.randint(0,2)
		if hunger < 3:
			current_state = 'ok'

	else:
		print("something is wrong, please check under the hood")
		exit()

	if hunger > 20:
		print("the FSM has died")
		running = False
	
	if game_time > max_limit:
 		running = False

print("goodbye")

